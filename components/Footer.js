import React from 'react';
import { View, Text } from 'react-native';

export default function Footer() {
  return (
    <View style={{ backgroundColor: "#f0f0f0", alignItems: 'center', padding: 4 }}>
      <Text style={{ fontSize: 12, fontWeight: 'normal', fontStyle: 'italic' }}>
        Ishchenko Andrii, ZIPZ 20-1
      </Text>
    </View>
  );
}
