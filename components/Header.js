import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

export default function Header() {
  return (
    <View style={styles.container}>
      <Image    
        style={styles.image}     
        source={{
          uri: 'https://upload.wikimedia.org/wikipedia/commons/c/ce/%D0%9B%D0%BE%D0%B3%D0%BE%D1%82%D0%B8%D0%BF_%D0%96%D0%B8%D1%82%D0%BE%D0%BC%D0%B8%D1%80%D1%81%D1%8C%D0%BA%D0%BE%D1%97_%D0%BF%D0%BE%D0%BB%D1%96%D1%82%D0%B5%D1%85%D0%BD%D1%96%D0%BA%D0%B8.png',
        }} 
      />
      <Text style={styles.text}>FirstMobileApp</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 50, 
    paddingTop: 10, 
    marginBottom: 10, 
    backgroundColor: 'white', 
    display: 'flex', 
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    alignItems: 'center'
  },
  image: {
    height: 40, 
    width: 140, 
    resizeMode: 'stretch'
  },
  text: { 
    fontSize: 18, 
    fontWeight: 'bold', 
    alignItems: 'center' 
  }
})
