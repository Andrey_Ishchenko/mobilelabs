import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { Text, SafeAreaView, StyleSheet, Platform, StatusBar } from 'react-native';
import { MaterialIcons } from 'react-native-vector-icons';

import Header from './Header';
import Footer from './Footer';

import Home from '../screens/Home';
import Gallery from '../screens/Gallery';
import Profile from '../screens/Profile';

const Tab = createMaterialTopTabNavigator();

const tabBarOptions = {
  activeTintColor: 'blue',
  labelStyle: {
    fontSize: 12,
  },
  showIcon: true,
  style: {
    backgroundColor: '#f0f0f0',
  },
};

const styles = StyleSheet.create({
  safeAreaTop: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
  }
});

export default function TopTabNavigator() {
  return (
    <SafeAreaView style={styles.safeAreaTop}>
      <Header />
      <Tab.Navigator
        initialRouteName="Home"
        tabBarOptions={tabBarOptions}
      >
        <Tab.Screen
          name="Home"
          component={() => <Home />}
          options={{
            tabBarLabel: ({ focused }) => (
              <Text style={{ color: focused ? 'blue' : 'black' }}>Home</Text>
            ),
            tabBarIcon: ({ focused }) => (
              <MaterialIcons
                name='home'
                size={24}
                color={focused ? 'blue' : 'black'}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Gallery"
          component={() => <Gallery />}
          options={{
            tabBarLabel: ({ focused }) => (
              <Text style={{ color: focused ? 'blue' : 'black' }}>Gallery</Text>
            ),
            tabBarIcon: ({ focused }) => (
              <MaterialIcons
                name='photo-library'
                size={24}
                color={focused ? 'blue' : 'black'}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Profile"
          component={() => <Profile />}
          options={{
            tabBarLabel: ({ focused }) => (
              <Text style={{ color: focused ? 'blue' : 'black' }}>Profile</Text>
            ),
            tabBarIcon: ({ focused }) => (
              <MaterialIcons
                name='person'
                size={24}
                color={focused ? 'blue' : 'black'}
              />
            ),
          }}
        />
      </Tab.Navigator>
      <Footer />
    </SafeAreaView>
  );
}
