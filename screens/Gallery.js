import React, { useState } from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import ImageCard from '../components/ImageCard';

export default function Gallery() {
  const [imageList, setImageList] = useState([
    'https://placekitten.com/80/80',
    'https://placekitten.com/81/81',
    'https://placekitten.com/82/81',
    'https://placekitten.com/83/83',
    'https://placekitten.com/84/84',
    'https://placekitten.com/85/84',
    'https://placekitten.com/82/86',

  ]);

  return (
    <ScrollView style={styles.container}>
      <View style={styles.imageGrid}>
        {imageList.map((imageUrl, index) => (
          <ImageCard key={index} imageUrl={imageUrl} />
        ))}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
  },
  imageGrid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
});