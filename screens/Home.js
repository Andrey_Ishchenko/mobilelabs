import React from 'react';
import { ScrollView, Text, StyleSheet } from 'react-native';
import NewsItem from '../components/NewsItem';

export default function Footer() {
  const [newsList, setNewsList] = React.useState([
    {
      id: 1,
      title: 'Breaking News 1',
      date: '2024-01-01',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
      image: 'https://placekitten.com/80/80',
    },
    {
      id: 2,
      title: 'Breaking News 2',
      date: '2024-01-02',
      description: 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      image: 'https://placekitten.com/81/81',
    },
    {
      id: 3,
      title: 'Breaking News 3',
      date: '2024-01-03',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
      image: 'https://placekitten.com/82/81',
    },
    {
      id: 4,
      title: 'Breaking News 4',
      date: '2024-01-04',
      description: 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      image: 'https://placekitten.com/83/83',
    },    
    {
      id: 5,
      title: 'Breaking News 5',
      date: '2022-01-05',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
      image: 'https://placekitten.com/84/84',
    },
    {
      id: 6,
      title: 'Breaking News 6',
      date: '2022-01-06',
      description: 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      image: 'https://placekitten.com/85/84',
    },    
    {
      id: 7,
      title: 'Breaking News 7',
      date: '2022-01-07',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
      image: 'https://placekitten.com/82/86',
    },
  ]);

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.newsTitle}>News</Text>
      {newsList.map((news) => (
        <NewsItem key={news.id} news={news} />
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
  },
  newsTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 6,
    marginBottom: 10,
  },
});